dl = [["Spacecraft transmitter power [W]",                   None],
      ["Down link frequency [GHz]",                          None],
      ["Spacecraft transmitter loss factor",                 None],
      ["Ground station receiver loss factor",                None],
      ["Spacecraft Tx antenna diameter [m]",                 None],
      ["Ground station Rx antena diameter [m]",              None],
      ["Spacecraft Tx antenna efficiency",                   None],
      ["Ground station Rx antenna efficiency",               None],
      ["Orbital altitude or distance from Earth [km]",       None]
      ]

def prompt(message, type, min_val=None, max_val=None):
    while True:
        user_input = raw_input(message + ": ")
        try:
            user_input = type(user_input)
            if min_val is not None and type is int or type is float:
                assert user_input > min_val
            if max_val is not None and type is int or type is float:
                assert user_input < max_val
            return user_input
        except:
            print "Wrong input"


def print_menu():
    for i, item in enumerate(dl):
        print str(i) + "\t" + item[0]
    selected_item = prompt("\nSelect input item", int, min_val=0, max_val=len(dl)-1)
    prompt(dl[selected_item][0], float)

print_menu()
prompt("Give me an int", int)
